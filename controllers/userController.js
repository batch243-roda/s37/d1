const Users = require("../models/Users");
const Courses = require("../models/Courses");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const { createToken, decode } = require("../auth");

const getProfile = async (req, res) => {
  const { id } = req.body;

  try {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).send("No such user found");
    }

    const user = await Users.findById({ _id: id });

    if (!user) {
      return res.status(404).send("No such user found");
    }
    user.password = "";

    return res.status(200).send(user);
  } catch (error) {
    console.log(error);
    res.status(400).send(error.message);
  }
};

// Check if email already exists
const checkEmailExists = async (req, res, next) => {
  const { email } = req.body;

  try {
    const user = await Users.find({ email });
    if (user.length <= 0) {
      next();
    }
    return res.send(`The ${email} is already taken, please use other email`);
  } catch (error) {
    console.log(error);
    res.status(400).send(error.message);
  }
};

const registerUser = async (req, res) => {
  const { firstName, lastName, email, password, mobileNo } = req.body;
  const newUser = new Users({
    firstName,
    lastName,
    email,
    password: await bcrypt.hash(password, 10),
    mobileNo,
  });

  try {
    await newUser.save();
    console.log("newUser");
    return res
      .status(200)
      .send(`Congragulations, User ${email} is registered successfully`);
  } catch (error) {
    console.log(error.message);
    res.status(400).send(error.message);
  }
};

// User Authentication
/**
 * Steps:
 *  1. Check the database if email is registered/exist.
 *  2. compare the password written by the client to the users password.
 */
const loginUser = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await Users.findOne({ email });
    if (!user) {
      return res.send(
        `Your email: ${email} is not yet registered. Please register first! `
      );
    }

    const match = await bcrypt.compare(password, user.password);
    if (!match) {
      throw Error("Incorrect password");
    }
    // create token
    const token = createToken(user);
    console.log(`\n accessToken: ${token}\n`);
    return res.status(200).send({ accessToken: token });
  } catch (error) {
    console.log(error.message);
    res.status(400).send(error.message);
  }
};

const profileDetails = async (req, res) => {
  // userData will be object that contains the id and email of the user that is currently logged in
  const userData = decode(req.headers.authorization);
  console.log(userData);
  try {
    const user = await Users.findById(userData.id);

    user.password = "Confidential";
    return res.status(200).send(user);
  } catch (error) {
    console.log(error.message);
    return res.send(error.message);
  }
};

const updateRole = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  const { userId } = req.params;

  try {
    if (!userData.isAdmin) {
      return res.status(400).send("Access Denied!, admin users only");
    }

    const userToBeUpdated = await Users.findById(userId);

    const updatedUser = await Users.findByIdAndUpdate(
      userId,
      {
        isAdmin: !userToBeUpdated.isAdmin,
      },
      { new: true }
    );

    updatedUser.password = "Confidential";
    return res.status(200).send(updatedUser);
  } catch (error) {
    console.log(error.message);
    return res.send(error.message);
  }
};

const enroll = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);
  const { courseId } = req.params;

  try {
    if (userData.isAdmin)
      return res.status(400).send("Access Denied!, admin users cannot enroll");

    const course = await Courses.findById(courseId);
    const user = await Users.findById(userData.id);

    if (!course) {
      return res.send("Course ID Invalid!");
    } else if (!user) {
      return res.send("User ID invalid");
    }

    course.enrollees.push({
      userId: userData.id,
    });
    course.slots--;
    user.enrollments.push(courseId);

    await course.save();
    await user.save();

    return res.status(200).send(`Enrolled Successfully`);
  } catch (error) {
    console.log(error.message);
    return res.send(error.message);
  }
};

module.exports = {
  checkEmailExists,
  registerUser,
  loginUser,
  getProfile,
  profileDetails,
  updateRole,
  enroll,
};
