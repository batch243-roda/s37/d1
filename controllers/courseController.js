const Courses = require("../models/Courses");
const mongoose = require("mongoose");
const { decode } = require("../auth");

// Add course
const addCourse = async (req, res) => {
  const { name, description, price, slots } = req.body;

  let newCourse = new Courses({
    name,
    description,
    price,
    slots,
  });

  const userData = decode(req.headers.authorization);
  console.log(`\n${userData}\n`);
  try {
    if (userData.isAdmin) {
      await newCourse.save();
      return res.send(`Course added successfully`);
    }

    return res.send(`You cannot add courses, Only admin can add course`);
  } catch (error) {
    console.log(error.message);
    res.send(false);
  }
};

// Get all active courses
const getActiveCourses = async (req, res) => {
  try {
    const course = await Courses.find({ isActive: true });

    return res.send(course);
  } catch (error) {
    console.log(error.message);
    return res.send(error.message);
  }
};

// Get specific active course
const getActiveCourse = async (req, res) => {
  const { courseId } = req.params;
  try {
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
      return res.status(400).send(`ID is invalid`);
    }

    const course = await Courses.findById({ _id: courseId });

    return res.status(200).send(course);
  } catch (error) {
    console.log(error.message);
    return res.status(400).send(`ID is invalid`);
  }
};

// Get all courses including inactive
const getCourses = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (!userData.isAdmin) {
      return res.send("Access Denied!. Only admin can access this page");
    }

    const course = await Courses.find({});

    return res.send(course);
  } catch (error) {
    console.log(error);
    return res.status(400).send(error.message);
  }
};

// Update all fields in documents
const updateCourse = async (req, res) => {
  const { name, description, price, slots } = req.body;
  const { courseId } = req.params;

  let updatedCourse = {
    name,
    description,
    price,
    slots,
  };

  const userData = decode(req.headers.authorization);

  try {
    if (userData.isAdmin) {
      if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).send(`ID is invalid`);
      }

      const course = await Courses.findByIdAndUpdate(
        { _id: courseId },
        updatedCourse,
        { new: true }
      );

      return res.status(200).send(course);
    }

    return res.status(400).send(`Prohibeted. User is not admin.`);
  } catch (error) {
    console.log(error.message);
    return res.status(400).send(`ID is invalid`);
  }
};

// archive the course making isActive = false
const archiveCourse = async (req, res) => {
  const { courseId } = req.params;

  const userData = decode(req.headers.authorization);

  try {
    if (userData.isAdmin) {
      if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).send(`ID is invalid`);
      }

      const course = await Courses.findByIdAndUpdate(
        { _id: courseId },
        { ...req.body },
        { new: true }
      );

      return res.status(200).send(course);
    }

    return res.status(400).send(`Access Denied!. User is not admin.`);
  } catch (error) {
    console.log(error.message);
    return res.status(400).send(`ID is invalid`);
  }
};

module.exports = {
  addCourse,
  getActiveCourses,
  getActiveCourse,
  getCourses,
  updateCourse,
  archiveCourse,
};
