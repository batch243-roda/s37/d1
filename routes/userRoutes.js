const express = require("express");
const router = express.Router();

const {
  checkEmailExists,
  registerUser,
  loginUser,
  getProfile,
  profileDetails,
  updateRole,
  enroll,
} = require("../controllers/userController");

const { verify } = require("../auth");

// Check email
router.post("/checkEmail", checkEmailExists);
// Register user
router.post("/register", checkEmailExists, registerUser);
// Login user
router.post("/login", loginUser);
// Check details
router.post("/details", verify, getProfile);
// Check profile
router.get("/profile", profileDetails);

router.patch("/:userId/updateRole", verify, updateRole);

router.post("/:courseId/enroll", verify, enroll);

module.exports = router;
