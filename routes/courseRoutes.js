const express = require("express");
const router = express.Router();

const {
  addCourse,
  getActiveCourses,
  getActiveCourse,
  updateCourse,
  archiveCourse,
  getCourses,
} = require("../controllers/courseController");

const { verify } = require("../auth");

// Add course
router.post("/add", verify, addCourse);
// Get all active courses
router.get("/activeCourses", getActiveCourses);
// Get specific active course
router.get("/allCourses", verify, getCourses);
// Get all courses including inactive
router.get("/:courseId/active", getActiveCourse);
// Update all fields in documents
router.put("/:courseId/update", verify, updateCourse);
// archive the course making isActive = false
router.patch("/:courseId/archive", verify, archiveCourse);

module.exports = router;
